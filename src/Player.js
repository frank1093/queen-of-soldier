var Player = cc.NodeRGBA.extend({
	ctor: function() {
		this._super();
		this.map = null;	
		this.statusWalk = null;
		this.line = Map.LINE.SECOND;
		this.blackColor = new cc.Color3B( 54, 54, 54 );
		this.setZOrder( 1 );
		
		// this.initWithFile( 'images/player.png');
		// this.setAnchorPoint(0,0);
		this.init();
		this.setZ();
		this.setUpCallMinionsSkill();
		this.setUpRecoverSkill();
		this.setUpAttackSpeedSkill();
		this.setUpAttackSkill();
		this.setUpDefenseSkill();
		


		this.scheduleUpdate();
		this.attacking = false;
		this.running = false;
		this.standing = true;
		this.lockAttacking = false;
		this.damageBase = 200;
		this.damageRange = 100;
		this.defense = 30;
		this.blinking = false;
		this.dead = false;
		this.point = 0;
		this.attackSpeed = Player.STATS.ATTACK_SPEED;

		this.holdAttack = false;
		
		

		this.showHealthPoint();
		PlayerLink = this;
		
	},
	// spriteSheet:null,
	runningAction:null,
	playerSprite:null,
	standingAction:null,
	attackingAction: null,
	direction : null,
	monsters: null,
	init:function () {
        // this._super();

        // create sprite sheet
        cc.SpriteFrameCache.getInstance().addSpriteFrames('images/player.plist');
        // this.spriteSheet = cc.SpriteBatchNode.create('images/player.png');
        // this.addChild(this.spriteSheet);

		this.playerSprite = cc.Sprite.createWithSpriteFrameName("stand1.png");
		// this.playerSprite.initWithSpriteFrameName( "stand1.png");
        this.playerSprite.setAnchorPoint( 0.5, 0 );
        this.playerSprite.setPosition( new cc.Point( 70*4, this.line ));


        // init action
        this.createStandingAction();
        this.createRunningAction();
        // this.createActtackingAction();

        this.playerSprite.runAction(this.standingAction);
        // this.spriteSheet.addChild(this.sprite);
        this.addChild( this.playerSprite );

    },
    createStandingAction: function() {
    	var animFrames = createAnimeFrames( 'stand', 1, 4 );
        var animation = cc.Animation.create(animFrames, 0.1);
        this.standingAction = cc.RepeatForever.create(cc.Animate.create(animation));
        this.standingAction.setTag('move');


        this.playerSprite.setFlippedX(true);

    },
    createRunningAction: function() {
    	var animFrames = createAnimeFrames( 'run', 1, 4);

        var animation = cc.Animation.create(animFrames, 0.1);
        this.runningAction = cc.RepeatForever.create(cc.Animate.create(animation));
        this.runningAction.setTag('move');
    },
    createActtackingAction: function() {
    	var animFramesBeforeAttacking = createAnimeFrames( 'attack', 1, 7);
        var animationBeforeAttacking = cc.Animation.create( animFramesBeforeAttacking, this.attackSpeed);
        var beforeAttackingAction = cc.Repeat.create(cc.Animate.create( animationBeforeAttacking ), 1);
        beforeAttackingAction.setTag( 'attack' );

		var animFramesAttacking = createAnimeFrames( 'attack', 8, 8);
        var animationAttacking = cc.Animation.create( animFramesAttacking, this.attackSpeed);
        var attackingAction = cc.Repeat.create( cc.Animate.create( animationAttacking ), 1);
        attackingAction.setTag( 'attack' );

        var animFramesAfterAttacking = createAnimeFrames( 'attack', 9, 10);
        var animationAfterAttacking = cc.Animation.create( animFramesAfterAttacking, this.attackSpeed);
        var afterAttackingAction = cc.Repeat.create( cc.Animate.create( animationAfterAttacking ), 1);
        afterAttackingAction.setTag( 'attack' );

        var attackingActions = [];
		attackingActions.push( beforeAttackingAction );
		attackingActions.push( cc.CallFunc.create( function(){ this.attacking = true; }, this) );
		// attackingActions.push( finish12 );
		// this.destroyMonster();
		attackingActions.push( cc.CallFunc.create( MonstersLink.attacked , MonstersLink ) ); //
		attackingActions.push( attackingAction );
		attackingActions.push( cc.CallFunc.create( function(){ this.attacking = false;}, this) );
		// attackingActions.push( cc.CallFunc.create( this.setAttacking ,) );
		attackingActions.push( afterAttackingAction );
		attackingActions.push( cc.CallFunc.create( this.setAfterAttack, this ) );
		this.attackingAction = cc.Sequence.create( attackingActions );
		this.attackingAction.setTag( 'attack' );

    },
	update: function ( dt ){
		this.dt = dt;
		this.checkOutOfMap();
		this.move( this.direction, dt);
		this.attackMonster();
	},
	move: function( drirection, dt){
		if (Player.DIR.UP == drirection){
			this.moveUp();
		}
		else if (Player.DIR.DOWN == drirection){
			this.moveDown();
		}
		if (Player.DIR.RIGHT == drirection){
			if ( this.statusWalk != Player.STATUS.BLOCKRIGHT ){
				// this.moveRight();
				// this.moveUpdate = this.moveRight();
				this.moveRight(dt);			}
		}
		else if (Player.DIR.LEFT == drirection ){
			this.moveLeft(dt);
		}
	},

	getRect: function () {
		return this.playerSprite.getBoundingBoxToWorld();
	},
	getMaxX: function() {
		return cc.rectGetMaxX( this.getRect() );
	},
	getMinX: function() {
		return cc.rectGetMinX( this.getRect() );
	},
	getMaxY: function() {
		return cc.rectGetMaxY( this.getRect() );
	},
	getMinY: function() {
		return cc.rectGetMinY( this.getRect() );
	},
	setMap: function( map ) {
		this.map = map ;
	},
	blockMoveRight: function() {
		this.statusWalk = Player.STATUS.BLOCKRIGHT;
	},
	allowMoveRight: function() {
		this.statusWalk = null;
	},
	reBound: function( rect ) {
		// if ( this.getStatusAttacking() ) return;
		var intersectionRect = cc.rectIntersection(rect, this.getRect());
		var backDistance = cc.rectGetMinX(intersectionRect)-cc.rectGetMaxX(intersectionRect) ;
		if ( cc.rectGetMaxX( rect ) < this.getMaxX() ){
			backDistance *= -1;
		}
		if (backDistance <= 0 ){
			this.playerSprite.setFlippedX( true );
		}
		else {
			this.playerSprite.setFlippedX( false );
		}
		var action = cc.MoveBy.create( this.dt, cc.p( backDistance, 0));
		this.playerSprite.runAction( action );
	},
	getLine: function() {
		return this.line;
	},
	showStandingState: function() {
		if (!this.attacking && !this.lockAttacking){
			// this.sprite.stopActionByTag('move');
			if ( !this.standing ){
				// this.playerSprite.stopAllActions();
				this.stopMoving();
				this.stopAttacking();
				// this.playerSprite.stopActionByTag('move');

				this.playerSprite.runAction( this.standingAction );
				this.running = false;
				this.standing = true;
			}			
			this.direction = null;
			this.holdAttack = false;
		}
	},
	setZ: function() {
		switch (this.line){
			case Map.LINE.FIRST: this.playerSprite.setZOrder(Map.ZOrder.FIRSTLINE);break;
			case Map.LINE.SECOND: this.playerSprite.setZOrder(Map.ZOrder.SECONDLINE);break;
			case Map.LINE.THRID: this.playerSprite.setZOrder(Map.ZOrder.THRIDLINE);break;
			case Map.LINE.FOURTH: this.playerSprite.setZOrder(Map.ZOrder.FOURTHLINE);break;
		}
	},
	attackMonster: function() {
		// this.holdKey = true;
		if ( !this.holdAttack ) return;
		if (!this.attacking && !this.lockAttacking ){
			// this.attacking = true;
			this.standing = false;
			this.lockAttacking = true;
			this.lockMoving = true;
			this.direction = null;
			this.holdAttack = false;
			// this.sprite.stopActionByTag('move');
			// this.playerSprite.stopAllActions();
			this.stopMoving();

			this.createActtackingAction();
			this.playerSprite.runAction( this.attackingAction );
			cc.AudioEngine.getInstance().playEffect( 'sounds/swordL.Attack.wav');
		}
	},
	getStatusAttacking: function() {
		return this.attacking;
	},
	moveUp: function() {
		this.attacking = false;
		this.direction = null;
		this.lockAttacking = false;
		if ( this.line == Map.LINE.FOURTH ){
			return;
		}
		this.line += Map.DISTANCE;
		this.playerSprite.setPositionY(this.line);
		this.setZ();
		this.showStandingState();
		// this.stopAllActions();

	},
	moveDown: function() {
		this.attacking = false;
		this.direction = null;
		this.lockAttacking = false;
		if ( this.line == Map.LINE.FIRST ){
			return;
		}
		this.line -= Map.DISTANCE;
		this.playerSprite.setPositionY(this.line);
		this.setZ();
		this.showStandingState();
	},
	moveLeft: function(dt) {
		// this.sprite.setAnchorPoint(1,0);
		this.attacking = false;
		this.standing = false;
		this.lockAttacking = false;
		var action = cc.MoveBy.create( 0.05, cc.p( -Player.WALK.HORIZONTAL, 0));
		this.playerSprite.runAction( action );
		if (!this.running){
			this.running = true;
			this.stopMoving();
			this.stopAttacking();
			
			this.playerSprite.runAction( this.runningAction );
		}
		// if ( this.isFlippedX() ){
			this.playerSprite.setFlippedX( false );
		// }
	},
	moveRight: function( dt ) {
		// console.log(dt);
		// this.holdKey = true;
		this.attacking = false;
		this.standing = false;
		this.lockAttacking = false;
		var action = cc.MoveBy.create( 0.05 , cc.p( Player.WALK.HORIZONTAL, 0));
		this.playerSprite.runAction( action );
		// this.rigth = true;
		// this.setPositionX( this.getPositionX() + 5 );

		if (!this.running){
			this.running = true;
			this.stopMoving();
			this.stopAttacking();
			// this.playerSprite.stopAllActions();
			this.playerSprite.runAction( this.runningAction );
			// console.log( this.playerSprite.getNumberOfRunningActions() );
		}
		// console.log( this.playerSprite.getNumberOfRunningActions() );
		// if ( !this.isFlippedX() ){
			this.playerSprite.setFlippedX( true );
		// }		
	},
	stopMoving: function() {
		this.playerSprite.stopActionByTag('move');
	},
	stopAttacking: function() {
		// console.log( this.getActionByTag('attack') );
		this.playerSprite.stopActionByTag('attack');
	},
	setDirection: function( direction ){
		this.direction = direction;
	},
	setAttacking: function( boolean ) {
		
		this.attacking = boolean;
		// console.log( boolean );
	},
	setAfterAttack: function( ) {
		this.attacking = false;
		this.lockAttacking = false; 
		this.createActtackingAction();
		this.showStandingState();
	},
    checkOutOfMap: function(){
	    if (this.getMaxX() > 980 ){
	        // this.lockCheckOutOfBound = true;
	        // this.setDirection( Player.DIR.LEFT );
	        this.playerSprite.runAction( cc.MoveBy.create( this.dt, cc.p(-Player.WALK.HORIZONTAL,0)));
	    }
	    else if (this.getMinX() < 0 ) {
	        // this.lockCheckOutOfBound = true;
	        // this.setDirection( Player.DIR.RIGHT );
	        this.playerSprite.runAction( cc.MoveBy.create( this.dt, cc.p(Player.WALK.HORIZONTAL,0)));
	    }

	},
	showHealthPoint: function() {
		this.HP = new HealthPoint( 300 );
		this.HP.setPositionX( -15);
		this.HP.setPositionY( - 14 );
		this.HP.setFullHeathPoint();
		this.playerSprite.addChild( this.HP );
	},
	isFlippedX: function() {
		return this.playerSprite.isFlippedX();
	},
	getDamage: function() {
		return this.damageBase + (Math.random()*2-1)*this.damageRange;
	},
	intersectSword: function() {
		
	},
	setMonsters: function( monsters ) {
		this.monsters = monsters;
	},
	deductHealthPoint: function( damage, rect){
		// console.log( cc.rectOverlapsRect( this.getRect(), rect) );
		cc.AudioEngine.getInstance().playEffect( 'sounds/10001066.Hit.wav');
		if ( cc.rectOverlapsRect( this.getRect() , rect) ){
			this.HP.reduce( damage - this.defense );
		}
		if ( PlayerLink.HP.getHealthPoint() <= 0 ){
			PlayerLink.reborn();
		}

	},
	blink: function( damage, flipped, rect) {
		if ( this.blinking || this.lockAttacking ){
			return;
		}
		this.blinking = true;
		this.playerSprite.setFlippedX( flipped );
		this.deductHealthPoint( damage, rect );
		if ( this.playerSprite.isFlippedX() ){
			this.playerSprite.runAction( cc.MoveBy.create( 0.3, cc.p( -20, 0 ) ) );
		}
		else {
			this.playerSprite.runAction( cc.MoveBy.create( 0.3, cc.p( 20, 0 ) ) );
		}
		

		var actions = [];
		actions.push( cc.Blink.create( 2, 10) );
		// actions.push( cc.DelayTime.create(2 ));
		actions.push( cc.CallFunc.create( function(){ this.blinking = false }, this) );
		this.playerSprite.runAction( cc.Sequence.create( actions ) );
		cc.AudioEngine.getInstance().playEffect( 'sounds/13101002.Hit.wav');
	},
	setUpCallMinionsSkill: function(){
		this.callMinionsSkillPic = cc.Sprite.create( 'images/CallMinionSkillPic.png');
		this.callMinionsSkillPicProgressTimer =  cc.ProgressTimer.create( cc.Sprite.create('images/CallMinionSkillPic_Frame.png') );
		this.callMinionsSkillPicProgressTimer.setColor( this.blackColor );
		this.callMinionsSkillPicProgressTimer.setOpacity( 160 );
		this.callMinionsSkillPicProgressTimer.setAnchorPoint( 0, 0);
		// this.callMinionsSkillPicProgressTimer.setPercentage( 100 );
		// var action = cc.ProgressFromTo.create( Player.COLDDOWNSKILL.CALL_MINIONS, 100, 0 );
		// this.callMinionsSkillPicProgressTimer.runAction( action );
		this.callMinionsSkillPic.addChild( this.callMinionsSkillPicProgressTimer );
		this.callMinionsSkillPic.setPositionX( 210 );
		this.callMinionsSkillPic.setAnchorPoint( 0, 0);



	},
	useCallMinionsSkill: function() {
		this.coldDownSkill( this.callMinionsSkillPicProgressTimer, Player.COLDDOWNSKILL.CALL_MINIONS, Player.TIME.CALL_MINIONS,
			function(){
				PlayerLink.createStoneBug();
		});

	},
	setUpRecoverSkill: function() {
		this.recoverSkillPic = cc.Sprite.create( 'images/RecoverHPSkillPic.png');
		this.recoverSkillPicProgressTimer =  cc.ProgressTimer.create( cc.Sprite.create('images/CallMinionSkillPic_Frame.png') );
		this.recoverSkillPicProgressTimer.setColor( this.blackColor );
		this.recoverSkillPicProgressTimer.setOpacity( 160 );
		this.recoverSkillPicProgressTimer.setAnchorPoint( 0, 0);
		// var action = cc.ProgressFromTo.create( Player.COLDDOWNSKILL.RECOVER_HP_PALYER, 100, 0 );
		// this.recoverSkillPicProgressTimer.runAction( action );
		this.recoverSkillPic.addChild( this.recoverSkillPicProgressTimer );
		this.recoverSkillPic.setPositionX( 140 );
		this.recoverSkillPic.setAnchorPoint( 0, 0);
	},
	useRecoverHPPlayerSkill: function() {
		if ( this.dead ) return;
		this.coldDownSkill( this.recoverSkillPicProgressTimer, Player.COLDDOWNSKILL.RECOVER_HP_PALYER, Player.TIME.RECOVER_HP_PALYER, 
			function(){
				PlayerLink.HP.increase( Player.SKILL.RECOVER_HP_PALYER , Player.TIME.RECOVER_HP_PALYER );
		});
		
	},
	coldDownSkill: function( progressTimer, coldTime, time, func){
		if ( progressTimer.getPercentage() != 0){
			cc.AudioEngine.getInstance().playEffect( 'sounds/12001003.Use.wav');
			return;
		}
		// progressTimer.setPercentage( 100 );
		var action1 = cc.ProgressFromTo.create( time, 0, 100 );
		var action2 = cc.ProgressFromTo.create( coldTime, 100, 0 );
		progressTimer.runAction( cc.Sequence.create( action1, action2) );
		func();
		cc.AudioEngine.getInstance().playEffect( 'sounds/1301005.Use.wav');
	},
	createStoneBug: function() {
		this.bugStones = [];
		for (var j=1;j<Map.HEIGHT-1;j++){
			var bugStone = new StoneBug();
			this.bugStones.push( bugStone );
			bugStone.setPosition( cc.p( 3*70  ,j*70 +70 ));
			bugStone.setZOrder( 5 - j);
			this.addChild( bugStone );
		}
		// this.playerSprite.setZOrder( 1 );
	},
	reborn: function() {
		this.dead = true;
		var actions = [];
		actions.push( cc.FadeOut.create( 2 ) );
		actions.push( cc.CallFunc.create( function(){
			this.line = Map.LINE.SECOND;
			this.playerSprite.setPosition( cc.p( 70*2, this.line ) );

		}, this) );
		actions.push( cc.FadeIn.create( 1 ) );
		actions.push( cc.CallFunc.create( function(){
			this.HP.increase( 200, 2);
			this.dead = false;
		}, this) );
		this.playerSprite.runAction( cc.Sequence.create(actions));


	},
	setUpAttackSpeedSkill: function() {
		this.attackSpeedSkillPic = cc.Sprite.create( 'images/AttackSpeedSkillPic.png');
		this.attackSpeedSkillProgressTimer =  cc.ProgressTimer.create( cc.Sprite.create('images/CallMinionSkillPic_Frame.png') );
		this.attackSpeedSkillProgressTimer.setColor( this.blackColor );
		this.attackSpeedSkillProgressTimer.setOpacity( 160 );
		this.attackSpeedSkillProgressTimer.setAnchorPoint( 0, 0);
		// var action = cc.ProgressFromTo.create( Player.COLDDOWNSKILL.RECOVER_HP_PALYER, 100, 0 );
		// this.recoverSkillPicProgressTimer.runAction( action );
		this.attackSpeedSkillPic.addChild( this.attackSpeedSkillProgressTimer );
		this.attackSpeedSkillPic.setPositionX( 280 );
		this.attackSpeedSkillPic.setAnchorPoint( 0, 0);
	},
	useAttackSpeedSkill: function() {
		if ( this.dead ) return;
		var player = this;
		this.coldDownSkill( this.attackSpeedSkillProgressTimer, Player.COLDDOWNSKILL.ATTACK_SPEED, Player.TIME.ATTACK_SPEED,
			function(){
				var actions = [];
				actions.push( cc.DelayTime.create( Player.TIME.ATTACK_SPEED ) );
				actions.push( cc.CallFunc.create( function(){
					this.attackSpeed = Player.STATS.ATTACK_SPEED;
				}, player) );
				player.attackSpeed -= Player.SKILL.ATTACK_SPEED;
				player.runAction( cc.Sequence.create( actions ) );
		});
		
	},
	setUpAttackSkill: function() {
		this.attackSkillPic = cc.Sprite.create( 'images/AttackSkillPic.png');
		this.attackSkillProgressTimer =  cc.ProgressTimer.create( cc.Sprite.create('images/CallMinionSkillPic_Frame.png') );
		this.attackSkillProgressTimer.setColor( this.blackColor );
		this.attackSkillProgressTimer.setOpacity( 160 );
		this.attackSkillProgressTimer.setAnchorPoint( 0, 0);
		// var action = cc.ProgressFromTo.create( Player.COLDDOWNSKILL.RECOVER_HP_PALYER, 100, 0 );
		// this.recoverSkillPicProgressTimer.runAction( action );
		this.attackSkillPic.addChild( this.attackSkillProgressTimer );
		this.attackSkillPic.setPositionX( 350 );
		this.attackSkillPic.setAnchorPoint( 0, 0);
	},
	useAttackSkill: function() {
		if ( this.dead ) return;
		var player = this;
		this.coldDownSkill( this.attackSkillProgressTimer, Player.COLDDOWNSKILL.ATTACK, Player.TIME.ATTACK,
			function(){
				var actions = [];
				actions.push( cc.DelayTime.create( Player.TIME.ATTACK ) );
				actions.push( cc.CallFunc.create( function(){
					this.damageBase = Player.STATS.ATTACK;
					player.damageRange = Player.STATS.ATTACK_RANGE;
					GameStatusLink.attText.setString( 'ATT: ' + PlayerLink.damageBase +' ± ' + PlayerLink.damageRange );
				}, player) );
				player.damageBase += Player.SKILL.ATTACK;
				player.damageRange = Player.SKILL.ATTACK_RANGE;
				GameStatusLink.attText.setString( 'ATT: ' + PlayerLink.damageBase +' ± ' + PlayerLink.damageRange );
				player.runAction( cc.Sequence.create( actions ) );
		});
		
	},
	setUpDefenseSkill: function() {
		this.defenseSkillPic = cc.Sprite.create( 'images/DefenseSkillPic.png');
		this.defenseSkillProgressTimer =  cc.ProgressTimer.create( cc.Sprite.create('images/CallMinionSkillPic_Frame.png') );
		this.defenseSkillProgressTimer.setColor( this.blackColor );
		this.defenseSkillProgressTimer.setOpacity( 160 );
		this.defenseSkillProgressTimer.setAnchorPoint( 0, 0);
		// var action = cc.ProgressFromTo.create( Player.COLDDOWNSKILL.RECOVER_HP_PALYER, 100, 0 );
		// this.recoverSkillPicProgressTimer.runAction( action );
		this.defenseSkillPic.addChild( this.defenseSkillProgressTimer );
		this.defenseSkillPic.setPositionX( 420 );
		this.defenseSkillPic.setAnchorPoint( 0, 0);
	},
	useDefenseSkill: function() {
		if ( this.dead ) return;
		var player = this;
		this.coldDownSkill( this.defenseSkillProgressTimer, Player.COLDDOWNSKILL.DEFENSE,  Player.TIME.DEFENSE,
			function(){
				var actions = [];
				actions.push( cc.DelayTime.create( Player.TIME.DEFENSE ) );
				actions.push( cc.CallFunc.create( function(){
					this.defense = Player.STATS.DEFENSE;
					// player.damageRange = Player.STATS.ATTACK_RANGE;
					GameStatusLink.defText.setString( 'DEF: ' + PlayerLink.defense );
				}, player) );
				player.defense += Player.SKILL.DEFENSE;
				// player.damageRange = Player.SKILL.ATTACK_RANGE;
				GameStatusLink.defText.setString( 'DEF: ' + PlayerLink.defense );
				player.runAction( cc.Sequence.create( actions ) );
		});
		
	},
	upgradeAttack:function() {
		if ( this.point <= 0 ) {
			cc.AudioEngine.getInstance().playEffect( 'sounds/12001003.Use.wav');
			return;
		}
		this.point -= 1;
		this.damageBase += 1;
		Player.STATS.ATTACK = this.damageBase;
		GameStatusLink.attText.setString( 'ATT: ' + PlayerLink.damageBase +' ± ' + PlayerLink.damageRange );
		GameStatusLink.pointText.setString( 'POINT: '+ PlayerLink.point );
		cc.AudioEngine.getInstance().playEffect( 'sounds/1301005.Use.wav');
	},
	upgradeDefence: function(){
		if ( this.point <= 0 ) {
			cc.AudioEngine.getInstance().playEffect( 'sounds/12001003.Use.wav');
			return;
		}
		this.point -= 1;
		this.defense += 1;
		Player.STATS.DEFENSE = this.defense;
		GameStatusLink.defText.setString( 'DEF: ' + this.defense );
		GameStatusLink.pointText.setString( 'POINT: '+ PlayerLink.point );
		cc.AudioEngine.getInstance().playEffect( 'sounds/1301005.Use.wav');
	},
	upgradeAttackRange: function() {
		if ( this.point <= 0 || this.damageRange <= 20) {
			cc.AudioEngine.getInstance().playEffect( 'sounds/12001003.Use.wav');
			return;
		}
		this.point -= 1;
		this.damageRange -= 1;
		Player.STATS.ATTACK = this.damageRange;
		GameStatusLink.attText.setString( 'ATT: ' + PlayerLink.damageBase +' ± ' + PlayerLink.damageRange );
		GameStatusLink.pointText.setString( 'POINT: '+ PlayerLink.point );
		cc.AudioEngine.getInstance().playEffect( 'sounds/1301005.Use.wav');
	}
});

Player.DIR = {
	UP :0,
	DOWN :1,
	RIGHT :2,
	LEFT :3
};
Player.WALK = {
	VERTICAL : 70,
	HORIZONTAL : 5,
};
Player.STATUS = {
	BLOCKLEFT: 0,
	BLOCKRIGHT: 1
};
Player.COLDDOWNSKILL = {
	CALL_MINIONS: 40,
	RECOVER_HP_PALYER: 10,
	ATTACK_SPEED: 15,
	ATTACK: 18,
	DEFENSE: 20,
};
Player.TIME = {
	CALL_MINIONS:10 ,
	RECOVER_HP_PALYER: 3,
	ATTACK_SPEED: 10,
	ATTACK: 10,
	DEFENSE: 8,
};
Player.SKILL = {
	CALL_MINIONS:200 ,
	RECOVER_HP_PALYER: 100,
	ATTACK_SPEED: 0.03,
	ATTACK: 200,
	ATTACK_RANGE: 20,
	DEFENSE: 150,
};
Player.STATS = {
	ATTACK_SPEED: 0.04,
	ATTACK: 200,
	ATTACK_RANGE: 100,
	DEFENSE: 30,
};

PlayerLink = null;
 
