var Annie = cc.NodeRGBA.extend({
	ctor: function( healthPoint ) {
		this._super();
		// this.initWithFile( "images/annie.png");
		// this.setAnchorPoint( 0, 0);
		// this.i = 1;
		this.healthPoint = healthPoint;
		this.hitPlayer = false;
		this.attacked = false;
		this.line = null;
		this.fadeOutAction = null;
		this.init();
		this.startRun();
		this.damageBase = 150;
		this.damageRange = 40;
		this.hitDamage = 15;
		this.experience = 40;
		this.score = 60;
		this.point = 2;
		this.angryStatus = false;
		this.attacking = false;
		this.lockAttacking = false;
		this.lockPause = false;
		this.lockBackForStoneBug = false;
		this.lockStopActionsForStoneBug = false;
		
		// console.log( this.healthPoint );
		
		this.dead = false;

		this.scheduleUpdate();
	},
	annieSprite: null,
	update: function ( dt ){
		this.checkOutOfMap();
	},
	startRun : function() {
		this.attacked = false;
		this.hitPlayer = false;
		this.angryStatus = false;

		this.randomLine();
		this.setZ();
		this.setStartPosition();
		this.HP.setFullHeathPoint();
		var fadeInAction = cc.FadeIn.create(0.2);
		// this.annieSprite.runAction( action );
		this.annieSprite.runAction( cc.Sequence.create( fadeInAction, cc.CallFunc.create( this.run, this)));
		// this.run();
	},
	run: function() {
		// this.annieSprite.stopAction( this.action );
		// console.log( this.getRect() );
		// console.log( this.attacked );
		// if (!this.attacked){
			this.action = cc.MoveBy.create( this.getMaxX()*(Math.random()*3*0.01+0.02), cc.p( -this.getMaxX()-0.02 , 0 ) );
			// this.action = cc.Sequence.create(cc.DelayTime.create(1),  action1 );
	        // this.runAction( finish );
	        // this.annieSprite.re
	        this.lockAttacking = false;
	        this.lockPause = false;
	        this.attacking = false;
	        this.annieSprite.setFlippedX( false );
	        // this.action.setTag( 'move' ); 
	        this.annieSprite.stopAllActions();
	        this.annieSprite.runAction( this.movingAction );
			this.annieSprite.runAction( this.action );
		// }
	},
	checkOutOfMap: function() {
		// console.log(this.getMaxX() < 0);
		if ( this.getMaxX() < 0 ){
			this.stop();
			this.annieSprite.setPositionX( GameLayer.WIDTH );
			this.run();
		}
	},
	getRect: function() {
		return this.annieSprite.getBoundingBoxToWorld();
	},
	getMaxX: function() {
		return cc.rectGetMaxX( this.getRect() );
	},
	getMinX: function() {
		return cc.rectGetMinX( this.getRect() );
	},
	getMaxY: function() {
		return cc.rectGetMaxY( this.getRect() );
	},
	getMinY: function() {
		return cc.rectGetMinY( this.getRect() );
	},
	stop: function() {
		// this.annieSprite.stopActionByTag( 'move' );
		this.annieSprite.stopAllActions();
		this.annieSprite.runAction( this.createStandingAction() );
	},
	hasMoving: function() {
		return this.action!= null;
	},
	setHitPlayer: function( boolean ) {
		this.hitPlayer = boolean;
		// console.log( this.getRect() );
	},
	getHitPlayer: function() {
		return this.hitPlayer;
	},
	randomLine: function() {
		switch (Math.floor(Math.random()*4)+1){
			case 1: this.line = Map.LINE.FIRST;break;
			case 2: this.line = Map.LINE.SECOND;break;
			case 3: this.line = Map.LINE.THRID;break;
			case 4: this.line = Map.LINE.FOURTH;break;
		}
	},
	getLine: function() {
		return this.line;
	},
	setZ: function() {
		switch (this.line){
			case Map.LINE.FIRST: this.annieSprite.setZOrder(Map.ZOrder.FIRSTLINE);break;
			case Map.LINE.SECOND: this.annieSprite.setZOrder(Map.ZOrder.SECONDLINE);break;
			case Map.LINE.THRID: this.annieSprite.setZOrder(Map.ZOrder.THRIDLINE);break;
			case Map.LINE.FOURTH: this.annieSprite.setZOrder(Map.ZOrder.FOURTHLINE);break;
		}
	},
	disappear: function() {
		if (this.HP.getHealthPoint() > 0 ){
			return;
		}
		// this.attacked = true;
		this.annieSprite.stopAllActions();
		// this.annieSprite.stopActionByTag( 'pause' );
		// this.addScore( this.score );
		// this.activeScore( this.score );
		GameStatusLink.addScore( this.score );
		GameStatusLink.activeScore( this.score );
		this.stop();
		this.fadeOutAction = cc.FadeOut.create(1);
		// this.annieSprite.runAction( this.fadeOutAction );
		// this.startRun();
		this.annieSprite.runAction( cc.Sequence.create( this.fadeOutAction, cc.CallFunc.create( function(){ this.removeFromParent( true);}, this )));
		// this.annieSprite.runAction( this.fadeOutAction );
		this.dead = true;
		PlayerLink.point += this.point;
		GameStatusLink.pointText.setString( 'POINT: '+ PlayerLink.point );


	},
	setStartPosition: function() {
		this.annieSprite.setPosition( cc.p(GameLayer.WIDTH+Math.floor(Math.random()*100)/3*10,this.line) );
	},
	init:function () {
        // this._super();

        // create sprite sheet
        cc.SpriteFrameCache.getInstance().addSpriteFrames('images/annie.plist');
        // this.spriteSheet = cc.SpriteBatchNode.create('images/annie.png');
        // this.addChild(this.spriteSheet);

        this.annieSprite = cc.Sprite.createWithSpriteFrameName("AnnieWalk1.png");
        // this.sprite.setAnchorPoint( 0.5, 0 );

        // this.initWithSpriteFrameName( "move1.png");
        // this.setScaleX(3);
		this.annieSprite.setAnchorPoint( 0.5, 0 );
        // init action
        // this.createStandingAction();
        // this.createRunningAction();
        // this.createActtackingAction();
        this.addChild( this.annieSprite );

        this.createMovingAction();


        // this.annieSprite.runAction(this.movingAction);
        // this.spriteSheet.addChild(this.sprite);

        this.HP = new HealthPoint( this.healthPoint );
		// this.
		this.HP.setPositionX( -10 );
		this.HP.setPositionY( - 14 );

		this.annieSprite.addChild( this.HP );

    },
    createMovingAction: function(){
    	var animFrames = createAnimeFrames( 'AnnieWalk', 1, 4);
        var animation = cc.Animation.create(animFrames, 0.1);
        this.movingAction = cc.RepeatForever.create(cc.Animate.create(animation));
        this.movingAction.setTag('move');
    },
    createAttackedAction: function(){
    	var animFrames = createAnimeFrames( 'AnnieAttacked', 1, 5);
        var animation = cc.Animation.create(animFrames, 0.1);
        var attackedAction = cc.Repeat.create( cc.Animate.create(animation) ,1 );
        attackedAction.setTag('attacked');
        return attackedAction;
    },
    createAttackAction: function(){
    	var actions = [];
    	// var animFrames = createAnimeFrames( 'attack', 1, 10);
    	var animFrames = createAnimeFrames( 'AnnieAttack', 1, 9);
        var animation = cc.Animation.create(animFrames, 0.1);
        var attackAction = cc.Repeat.create( cc.Animate.create(animation) ,1 );

        var animFrames2 = createAnimeFrames( 'AnnieAttack', 10, 10);
        var animation2 = cc.Animation.create(animFrames2, 0.1);
        var attackAction2 = cc.Repeat.create( cc.Animate.create(animation2) ,1 );
        // attackedAction.setTag('attack');
        actions.push( attackAction );
        actions.push( cc.CallFunc.create( function(){ this.attacking = true;},this ) );
        if ( this.annieSprite.isFlippedX() ){
        	actions.push( cc.MoveBy.create( 0.1, cc.p( 20, 0 ) ) );

        }
        else {
        	actions.push( cc.MoveBy.create( 0.1, cc.p( -20, 0 ) ) );
    	}
        actions.push( attackAction2 );

        actions.push( cc.CallFunc.create( function(){ 
        	if ( PlayerLink.getLine() == this.line ){
        		PlayerLink.deductHealthPoint( this.getDamage() ,this.getRect() );
        	}
        }, this) );
        actions.push( cc.CallFunc.create( function(){ this.attacking = false;},this ) );
        // actions.setTag( 'attack' );
        var sequenceAction = cc.Sequence.create( actions );
        sequenceAction.setTag( 'attack' )
        return sequenceAction; 
    },
    createAttackCoreStone: function() {
    	var actions = [];
    	// var animFrames = createAnimeFrames( 'attack', 1, 10);
    	var animFrames = createAnimeFrames( 'AnnieAttack', 1, 9);
        var animation = cc.Animation.create(animFrames, 0.1);
        var attackAction = cc.Repeat.create( cc.Animate.create(animation) ,1 );

        var animFrames2 = createAnimeFrames( 'AnnieAttack', 10, 10);
        var animation2 = cc.Animation.create(animFrames2, 0.1);
        var attackAction2 = cc.Repeat.create( cc.Animate.create(animation2) ,1 );
        // attackedAction.setTag('attack');
        actions.push( attackAction );
        actions.push( cc.CallFunc.create( function(){ this.attacking = true;},this ) );
        actions.push( attackAction2 );
        actions.push( cc.CallFunc.create( function(){ CoreStoneLink.deductHealthPoint( this.getDamage(), this.damageBase, this.damageRange ) }, this) );
        actions.push( cc.CallFunc.create( function(){ this.attacking = false;},this ) );
        // actions.setTag( 'attack' );
        var sequenceAction = cc.Sequence.create( actions );
        sequenceAction.setTag( 'attack' )
        return sequenceAction; 
    },
    createStandingAction: function() {
    	var animFrames = createAnimeFrames( 'AnnieStand', 1, 5);
        var animation = cc.Animation.create(animFrames, 0.1);
        var action = cc.Repeat.create( cc.Animate.create(animation) ,1 );
        action.setTag( 'stand' );
        return action;
    },
    getStatusAttacked: function() {
    	return this.attacked;
    },
    reduce: function( damge , baseDamge, damageRange) {
  //   	if (this.HP.getHealthPoint() < 0 ){
		// 	return;
		// }
    	this.lockAttacking = true;
    	this.lockPause = true;

    	// this.lockPause = false;

		// this.annieSprite.stopActionByTag( 'move' );
    	// this.annieSprite.stopActionByTag( 'attack' );
    	this.attacking = false;
    	// th
    	// this.annieSprite.stopActionByTag( 'attacked' ); 
    	this.attacked = false;
    	// this.annieSprite.stopActionByTag( 'pause' );
		this.annieSprite.stopAllActions();
    	// this.pause();

    	
    	// this.annieSprite.runAction( cc.Sequence.create( this.createAttackedAction(), cc.CallFunc.create( function(){ this.annieSprite.runAction(this.movingAction) }, this) ) );
    	var actions1 = [];
    	// actions1.push( cc.CallFunc.create( function(){this.setAttacked(true);}, this) );
    	actions1.push( cc.CallFunc.create( function(){this.attacked = true;}, this) );
    	actions1.push( this.createAttackedAction() );
    	actions1.push( cc.CallFunc.create( function(){this.attacked = false;}, this) );
    	// actions1.push( cc.CallFunc.create( function(){this.angryStatus = true;}, this) );
    	// actions1.push( cc.CallFunc.create( function(){ this.annieSprite.runAction(this.movingAction); }, this) );
    	actions1.push( cc.CallFunc.create( this.run, this ) );
    	// actions1.push( cc.CallFunc.create( this.disappear, this ) );
    	var attackedAction = cc.Sequence.create( actions1 );
    	attackedAction.setTag( 'attacked' );
    	this.annieSprite.runAction( attackedAction );
    	// this.annieSprite.runAction( this.temp );


    	var actions = [];
    	var walkBackAction = cc.MoveBy.create( 0.1, cc.p( 5, 0 ) );
    	var walkForwardAction = cc.MoveBy.create( 0.1, cc.p( -5, 0 ) );
    	actions.push( walkBackAction );
    	actions.push( walkForwardAction );
    	var walkAction = cc.Sequence.create( actions );
    	walkAction.setTag( 'attacked' );
    	this.annieSprite.runAction( walkAction );

    	// this.attacked = true;
    	this.angryStatus = true;
    	// var damage = number + (Math.random()*2-1)*numberRange;
    	// console.log( damge );
    	this.HP.reduce( damge );
    	this.showDamage( damge , baseDamge, damageRange);
    	cc.AudioEngine.getInstance().playEffect( 'sounds/10001066.Hit.wav');
    	// this.attacked = false;
    	this.disappear();
    },
	showDamage: function( damage, baseDamge, damageRange) {
		// console.log( this.getMaxX() +"   "+  this.getMinY());
		var damage = new Damage( damage, this.getMaxX(), this.getMinY(), baseDamge, damageRange);
		// damage.setZOrder(  );
		this.addChild( damage );
	},
	setAttacked: function( boolean ){
		this.attacked = boolean;
	},
	pause: function() {
		if ( this.angryStatus ){
			return;
		}
		if ( !this.lockPause ){
		this.lockPause = true;
		this.lockAttacking = false;
		this.annieSprite.stopAllActions();
		// this.annieSprite.stopActionByTag( 'pause' );
		// this.annieSprite.stopActionByTag( 'move' );
		// var pauseAction = cc.Sequence.create( cc.DelayTime.create( 1 ),  cc.CallFunc.create( this.run, this ) );
		// var pauseAction = cc.Sequence.create( cc.CallFunc.create( this.run, this ), cc.DelayTime.create( 1 ) );
		var pauseAction = cc.Sequence.create( this.createStandingAction(), cc.CallFunc.create( this.run, this ) );
		pauseAction.setTag( 'pause' );
		this.annieSprite.runAction( pauseAction );
		}
	},
	getDamage: function() {
		return (Math.random()*2-1)*this.damageRange + this.damageBase;
	},
	getHitDamage: function() {
		return this.hitDamage;
	},
	attackPlayer: function() {
		// if (this.HP.getHealthPoint() < 0 ){
		// 	return;
		// }
		if ( this.lockAttacking ){
			return;
		}
		if ( this.attacking || this.attacked){
			return;
		}

		// this.annieSprite.stopActionByTag( 'move' );

		this.lockAttacking = true;

		this.annieSprite.stopAllActions();
		var actions = [];
		actions.push( this.createAttackAction() );
		actions.push( cc.CallFunc.create( function(){ this.lockAttacking = false;}, this ) );
		actions.push( cc.CallFunc.create( this.run, this ) );
		// cc.Sequence.create(  this.createAttackAction(), cc.CallFunc.create( function(){ this.lockAttacking = false;}, this));
		// actions.setTag( 'attack' );
		var sequenceAction = cc.Sequence.create( actions );
		this.annieSprite.runAction( sequenceAction );
		cc.AudioEngine.getInstance().playEffect( 'sounds/barehands.Attack.wav');

		// var moveAction = cc.MoveBy.create( 0.2, cc.p( -10, 0 ) );
		// moveAction.setTag( 'attack' );
		// this.annieSprite.runAction( moveAction );
	},
	attackCoreStone: function() {
		if ( this.lockAttacking ){
			return;
		}
		if ( this.attacking || this.attacked){
			// console.log(this.attacking);
			return;
		}

		// this.annieSprite.stopActionByTag( 'move' );

		this.lockAttacking = true;

		this.annieSprite.stopAllActions();
		var actions = [];
		actions.push( this.createAttackCoreStone() );
		// actions.push( cc.CallFunc.create( function(){ this.deductHealthPointCoreStone( this.getDamage() ) }, this) );
		actions.push( cc.CallFunc.create( function(){ this.lockAttacking = false;}, this ) );
		actions.push( cc.CallFunc.create( this.run, this ) );
		// cc.Sequence.create(  this.createAttackAction(), cc.CallFunc.create( function(){ this.lockAttacking = false;}, this));
		// actions.setTag( 'attack' );
		var sequenceAction = cc.Sequence.create( actions );
		this.annieSprite.runAction( sequenceAction );
		cc.AudioEngine.getInstance().playEffect( 'sounds/barehands.Attack.wav');

		// this.deductHealthPointCoreStone( this.getDamage() );

		// var moveAction = cc.MoveBy.create( 0.2, cc.p( -10, 0 ) );
		// moveAction.setTag( 'attack' );
		// this.annieSprite.runAction( moveAction );
	},

	getScore: function() {
		return this.score;
	},

	
})