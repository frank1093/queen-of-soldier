var GameStatus = cc.LayerColor.extend({
	ctor: function() {
		this._super();
        this.setZOrder( 2 );
		GameStatusLink = this;
        this.scoreForTenSecs = 5;
		

		this.initialTime();
		this.startTime();
		this.initialScore();
        this.showGameStart();
        this.showPlayerBar();
        this.showPointText();
        this.setUpRateScoreForEveryTenSecs();






        this.addChild( PlayerLink.callMinionsSkillPic );
        this.addChild( PlayerLink.recoverSkillPic );
        this.addChild( PlayerLink.attackSpeedSkillPic);
        this.addChild( PlayerLink.attackSkillPic );
        this.addChild( PlayerLink.defenseSkillPic );

	},
	initialTime: function() {
		this.time = cc.LabelBMFont.create(  '00:00','images/time.fnt');        
        this.time.setPosition( cc.p(470,380+70)) ;
        this.addChild( this.time );
        this.min = 0;
        this.sec = 0;
	},
	startTime: function() {
		this.schedule( function(){
           	this.increaseTime();
			this.addScore( this.scoreForTenSecs );
            this.activeScore( this.scoreForTenSecs );
        }, 1, cc.RepeatForever, 4.5);
	},
	increaseTime: function() {
        this.sec++;
        this.setUpTime();
      	// sec += this.sec;
      	// min += this.min;

     	// this.time.setString( min + ":" + sec );
	},
	setUpTime: function() {
		var min = "";
        var sec = "";
        if (this.sec == 60 ){
        	this.sec =0;
       		this.min ++;
     	}
   		if ( this.sec < 10 ){
      		sec += "0";
       	}
   		if ( this.min < 10 ){
    		min += "0";
    	}
      	sec += this.sec;
      	min += this.min;
      	this.time.setString( min + ":" + sec );
	},
	initialScore: function() {
		this.mark = 0;
        // this.count = 0;
        this.score = cc.LabelBMFont.create('score: 0', 'images/score.fnt');
        this.score.setAnchorPoint( cc.p( 0, 0 ) );
        this.score.setPosition( cc.p(780,385+70)) ;
        this.addChild( this.score );
	},
	addScore: function( amount ){
        // if ( this.count == 0 ){
        //     this.count ++;
        //     this.activeScore( amount );
        // }
        if ( amount == 0 ){
            // this.count = 0;
            return;
        }
        this.mark += 1;
        this.score.setString( "score: " + this.mark );
        var action = cc.Sequence.create( cc.DelayTime.create( 0.01 ), cc.CallFunc.create( function(){ this.addScore( amount - 1 ); }, this ) );
        this.runAction( action );
    },
    activeScore: function( amount ){
        var score = cc.LabelBMFont.create( '+' + amount, 'images/score.fnt');
        score.setPosition( cc.p(910,380 + 70 ) );
        score.setVisible( false );
        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create(0.1) );
        actions.push( cc.MoveBy.create( 0.2, cc.p( 0, 20 ) ) );
        actions.push( cc.FadeOut.create(0.5) );
        // actions.push( cc.Hide.create() );
        actions.push( cc.CallFunc.create( score.removeFromParent, score, true));
        score.runAction( cc.Sequence.create( actions ) );
        this.addChild( score );
    },
    showGameStart: function() {
        cc.AudioEngine.getInstance().playEffect( 'sounds/5th_Maple.gaga.wav');
        this.gameStartText = cc.LabelBMFont.create(  'GAME START','images/game_start.fnt');
        this.gameStartText.setAnchorPoint( 0.5, 0.5 );
        this.gameStartText.setPosition( GameLayer.WIDTH/2, GameLayer.HEIGHT/2);
        this.gameStartText.setVisible( false );


        var actions = [];
        var actions2 = [];
        actions.push( cc.DelayTime.create( 2 ));
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 0.5 ) );
        actions2.push( cc.DelayTime.create( 2 ));
        actions2.push( cc.MoveBy.create( 1.5, cc.p( 0, 30 ) ) );
        actions2.push( cc.FadeOut.create(1) );
        this.gameStartText.runAction( cc.Sequence.create( actions ) );
        this.gameStartText.runAction( cc.Sequence.create( actions2 ) );
        this.addChild( this.gameStartText );
    },
    showSkillText: function() {
        this.skillText = cc.LabelBMFont.create(  'SKILL:','images/player_status.fnt'); 
        this.skillText.setAnchorPoint( 0 , 0);
        this.skillText.setPosition( cc.p( 10, 0 ) );
        this.addChild( this.skillText);
    },
    showStatusText: function() {
        this.statusText = cc.LabelBMFont.create(  'STATUS:','images/player_status.fnt'); 
        this.statusText.setAnchorPoint( 0 , 0);
        this.statusText.setPosition( cc.p( 500, 0 ) );
        this.addChild( this.statusText);
    }, 
    showSTRText: function() {
        this.STRText = cc.LabelBMFont.create(  'STR: 10','images/player_status2.fnt'); 
        this.STRText.setAnchorPoint( 0 , 0);
        this.STRText.setPosition( cc.p( 690, 30 ) );
        this.addChild( this.STRText);
    },
    showVITText:function() {
        this.VITText = cc.LabelBMFont.create(  'VIT: 0','images/player_status2.fnt'); 
        this.VITText.setAnchorPoint( 0 , 0);
        this.VITText.setPosition( cc.p( 790, 30 ) );
        this.addChild( this.VITText);
    },
    showINTText: function() {
        this.INTText = cc.LabelBMFont.create(  'INT: 0','images/player_status2.fnt'); 
        this.INTText.setAnchorPoint( 0 , 0);
        this.INTText.setPosition( cc.p( 890, 30 ) );
        this.addChild( this.INTText);
    },
    showAGIText: function() {
        this.AGIText = cc.LabelBMFont.create(  'AGI: 0','images/player_status2.fnt'); 
        this.AGIText.setAnchorPoint( 0 , 0);
        this.AGIText.setPosition( cc.p( 690, 0 ) );
        this.addChild( this.AGIText);
    },
    showDEXText: function() {
        this.DEXText = cc.LabelBMFont.create(  'DEX: 0','images/player_status2.fnt'); 
        this.DEXText.setAnchorPoint( 0 , 0);
        this.DEXText.setPosition( cc.p( 790, 0 ) );
        this.addChild( this.DEXText);
    },
    showLUKText: function() {
        this.LUKText = cc.LabelBMFont.create(  'LUK: 0','images/player_status2.fnt'); 
        this.LUKText.setAnchorPoint( 0 , 0);
        this.LUKText.setPosition( cc.p( 890, 0 ) );
        this.addChild( this.LUKText);
    },
    showStatusBar: function(){
        this.showStatusText();
        // this.showSTRText();
        // this.showVITText();
        // this.showINTText();
        // this.showAGIText();
        // this.showDEXText();
        // this.showLUKText();
        this.showAttText();
        this.showDefText();
    },
    showPlayerBar: function(){
        this.showSkillText();
        this.showStatusBar();
    },
    showGameOver: function(){
        console.log( 'ssssss' );
        cc.AudioEngine.getInstance().playEffect( 'sounds/Loose.wav');
        this.gameOverText = cc.LabelBMFont.create(  'GAME OVER','images/game_start.fnt');
        this.gameOverText.setAnchorPoint( 0.5, 0.5 );
        this.gameOverText.setPosition( GameLayer.WIDTH/2, GameLayer.HEIGHT/2);
        this.gameOverText.setVisible( false );


        this.blackBackground = cc.LayerColor.create(cc.c4b(0,0,0,255));
        this.blackBackground.setZOrder( 2 );
        this.blackBackground.setVisible( false );
        this.addChild( this.blackBackground );

        var actions = [];
        var actions2 = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 0.5 ) );
        actions2.push( cc.MoveBy.create( 1.5, cc.p( 0, 30 ) ) );
        actions2.push( cc.FadeOut.create(1) );
        actions2.push( cc.CallFunc.create( function(){  
            this.blackBackground.runAction( cc.Sequence.create( cc.Show.create(), cc.FadeIn.create( 2 ) ) );
        }, this ) );
        this.gameOverText.runAction( cc.Sequence.create( actions ) );
        this.gameOverText.runAction( cc.Sequence.create( actions2 ) );
        this.addChild( this.gameOverText );

        this.blackBackground.runAction( cc.Sequence.create( cc.DelayTime.create( 4.5 ), cc.CallFunc.create( function(){
            changeToScoreLayer(this);}, this )));
        
    },
    showAttText: function() {
        this.attText = cc.LabelBMFont.create(  'ATT: ' + PlayerLink.damageBase +' ± ' + PlayerLink.damageRange,'images/player_status2.fnt'); 
        this.attText.setAnchorPoint( 0 , 0);
        this.attText.setPosition( cc.p( 690, 30 ) );
        this.addChild( this.attText);
    },
    showDefText: function() {
        this.defText = cc.LabelBMFont.create(  'DEF: ' + PlayerLink.defense,'images/player_status2.fnt'); 
        this.defText.setAnchorPoint( 0 , 0);
        this.defText.setPosition( cc.p( 690, 0 ) );
        this.addChild( this.defText);
    },
    showPointText: function() {
        this.pointText = cc.LabelBMFont.create(  'POINT: ' + PlayerLink.point,'images/player_status2.fnt'); 
        this.pointText.setAnchorPoint( 0 , 0);
        this.pointText.setPosition( cc.p( 850, 0 ) );
        this.addChild( this.pointText);
    },
    setUpRateScoreForEveryTenSecs: function(){
        this.schedule( function(){
            this.scoreForTenSecs += 1;
        }, 10 , cc.RepeatForever, 4.5 +10);
    }





});

GameStatusLink = null;
// GameLayer.scoreForTenSecs 
// GameStatus.scoreBoard = null;


