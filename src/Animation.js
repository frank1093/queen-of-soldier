var createAnimeFrames = function( name, startNumber, endNumber) {
	var animFrames = [];
    for (var i = startNumber; i <= endNumber; i++) {
        var str = name  + i + '.png';
        var frame = cc.SpriteFrameCache.getInstance().getSpriteFrame(str);
        animFrames.push(frame);
    }
    return animFrames;
};