var Monsters = cc.Node.extend({
	ctor: function() {
		this._super();
		MonstersLink = this;
		this.createAnnie();
		this.scheduleUpdate();
	},
	update: function() {
		this.hitMonster();
		this.attackCoreStone();
	},

	createAnnie: function() {
		this.annies = [];
		var actions = [];
		actions.push( cc.DelayTime.create( 3 ) );
		actions.push( cc.CallFunc.create( function(){
			for (var i = 0; i < 5; i++ ){
				var annie = new Annie(600);
				annie.damageBase = 150;
				annie.damageRange = 40;
				annie.hitDamage = 100;
				annie.score = 60;
				annie.point = 2;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 15 ) );
		actions.push( cc.CallFunc.create( function(){
			// this.annies = [];
			for (var i = 0; i < 5; i++ ){
				var annie = new Annie(800);
				annie.damageBase = 150;
				annie.damageRange = 50;
				annie.hitDamage = 100;
				annie.score = 70;
				annie.point = 3;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 15 ) );
		actions.push( cc.CallFunc.create( function(){
			// this.annies = [];
			for (var i = 0; i < 7; i++ ){
				var annie = new Annie(800);
				annie.damageBase = 170;
				annie.damageRange = 50;
				annie.hitDamage = 130;
				annie.score = 70;
				annie.point = 4;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
			// this.annies = [];
			for (var i = 0; i < 10; i++ ){
				var annie = new Annie(800);
				annie.damageBase = 170;
				annie.damageRange = 20;
				annie.hitDamage = 140;
				annie.score = 70;
				annie.point = 4;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
			// this.annies = [];
			for (var i = 0; i < 4; i++ ){
				var annie = new Annie(2000);
				annie.damageBase = 200;
				annie.damageRange = 10;
				annie.hitDamage = 300;
				annie.score = 100;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
			// this.annies = [];
			for (var i = 0; i < 7; i++ ){
				var annie = new Annie(1500);
				annie.damageBase = 200;
				annie.damageRange = 10;
				annie.hitDamage = 150;
				annie.score = 120;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
		}, this) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
		// this.annies = [];
			for (var i = 0; i < 8; i++ ){
				var annie = new Annie(1500);
				annie.damageBase = 200;
				annie.damageRange = 10;
				annie.hitDamage = 190;
				annie.score = 150;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
			}, this) );
		// this.runAction( cc.Sequence.create( actions ) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
		// this.annies = [];
			for (var i = 0; i < 8; i++ ){
				var annie = new Annie(2500);
				annie.damageBase = 200;
				annie.damageRange = 10;
				annie.hitDamage = 250;
				annie.score = 200;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
			}, this) );
		// this.runAction( cc.Sequence.create( actions ) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
		// this.annies = [];
			for (var i = 0; i < 10; i++ ){
				var annie = new Annie(3500);
				annie.damageBase = 200;
				annie.damageRange = 10;
				annie.hitDamage = 250;
				annie.score = 250;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
			}, this) );
		// this.runAction( cc.Sequence.create( actions ) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
		// this.annies = [];
			for (var i = 0; i < 8; i++ ){
				var annie = new Annie(4000);
				annie.damageBase = 400;
				annie.damageRange = 10;
				annie.hitDamage = 250;
				annie.score = 300;
				annie.point = 5;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
			}, this) );
		// this.runAction( cc.Sequence.create( actions ) );
		actions.push( cc.DelayTime.create( 20 ) );
		actions.push( cc.CallFunc.create( function(){
		// this.annies = [];
			for (var i = 0; i < 4; i++ ){
				var annie = new Annie(10000);
				annie.damageBase = 4000;
				annie.damageRange = 10;
				annie.hitDamage = 2500;
				annie.score = 3000;
				annie.point = 50;
				annie.angryStatus = true;
				this.addChild(annie);
				this.annies.push(annie);
			}			
			}, this) );
		this.runAction( cc.Sequence.create( actions ) );
	},

	hitMonster: function( ) {
		if ( PlayerLink.dead ) return;
		for (var i = 0; i < this.annies.length ; i++ ){
			var monster = this.annies[i];
			// console.log(monster.dead + "  " +i);
			if ( !monster.dead ){
				this.setUpHitMonster( monster );
			}
		}
	},
	checkPlayerInFront: function( monster ){
		// console.log("a " +  this.player.getMaxX() - monster.getMaxX() <= 0 && this.player.sprite.isFlippedX());
		return PlayerLink.getMaxX() - monster.getMaxX() <= 0 && PlayerLink.isFlippedX();
		// return this.player.sprite.isFlippedX();
	},

	checkPlayerInBack: function( monster ){
		// console.log("b " + this.player.getMaxX() - monster.getMaxX() >= 0 && !this.player.sprite.isFlippedX() );
		return PlayerLink.getMaxX() - monster.getMaxX() >= 0 && !PlayerLink.isFlippedX();
		// return !this.player.sprite.isFlippedX();
	},

	checkLine: function( monster ) {
		return monster.getLine() == PlayerLink.getLine() 
	},

	isOverlab: function( monster ) {
		return cc.rectOverlapsRect( monster.getRect(),PlayerLink.getRect());
	},
	attacked: function() {
		
		for (var i = 0; i < this.annies.length; i++ ){

		// var monster = this.annies[i];
		if (this.checkLine( this.annies[i] ) && !this.annies[i].dead && !PlayerLink.dead) {
			if ( this.isOverlab( this.annies[i] ) ){

		// if ( this.player.getStatusAttacking() ){
				if ( this.checkPlayerInFront( this.annies[i]) || this.checkPlayerInBack( this.annies[i] ) ){
					// console.log( "DDDDAAAA" ); 
					// console.log( "dddddd");
					// if ( !this.annies[i].getStatusAttacked() ){
						this.annies[i].reduce( PlayerLink.getDamage(), PlayerLink.damageBase, PlayerLink.damageRange);
						return;
						// return ;
				// }			
			}
		// }
		}
		}
	}
	},
	setUpAfterHit: function( monster ) {
		// this.player.allowMoveRight();
		// monster.setHitPlayer( false );
		// monster.stop();
		// monster.run();
	},
	setUpAfterOverLab: function( monster ) {
		// monster.stop();
		monster.pause();
		// monster.setHitPlayer( true );
		// this.player.blockMoveRight();
		// this.player.reBound( monster.getRect() );
	},
	checkFrontSpace: function( monster ){
		if (  cc.rectOverlapsRect(PlayerLink.getRect(), monster.getRect()) 
			&& cc.rectGetMidX(monster.getRect()) - cc.rectGetMidX(PlayerLink.getRect()) > 0){
			return true;
		}
		return false;
	},
	checkBackSpace: function( monster ){
		if (  cc.rectOverlapsRect(PlayerLink.getRect(), monster.getRect()) 
			&& cc.rectGetMidX(monster.getRect()) - cc.rectGetMidX(PlayerLink.getRect()) < 0){
			return true;
		}
		return false;
	},
	setUpHitMonster: function( monster ) {
		if ( this.checkLine( monster ) ){
			if ( this.isOverlab( monster ) && !PlayerLink.getStatusAttacking() ){
				// this.player.deductHealthPoint( monster.getDamage() );
				PlayerLink.blink( monster.getHitDamage(), this.checkSide( monster ) , monster.getRect());
				this.setUpAfterOverLab( monster );			
			} 
			if ( this.checkFrontSpace(monster) && monster.angryStatus ){
				monster.annieSprite.setFlippedX( false );
				monster.attackPlayer();
			}
			else if ( this.checkBackSpace(monster) && monster.angryStatus ){
				monster.annieSprite.setFlippedX( true );
				monster.attackPlayer();
			}			
		}
	},
	checkLeftSide: function( monster ) {
		return cc.rectGetMidX(monster.getRect()) - cc.rectGetMidX(PlayerLink.getRect()) > 0;
	},
	checkRightSide: function( monster ) {
		return cc.rectGetMidX(monster.getRect()) - cc.rectGetMidX(PlayerLink.getRect()) < 0;
	},
	checkSide: function( monster ){
		if ( this.checkLeftSide( monster ) ){
			return true;
		}
		else {
			return false;
		}
	},
	attackCoreStone: function(){
	for (var i = 0; i < this.annies.length; i++ ){
		var monster = this.annies[i];
		this.setUpAttackCoreStone( monster );
	}
	},
	setUpAttackCoreStone: function( monster ){
		if ( cc.rectOverlapsRect( CoreStoneLink.getRect(), monster.getRect() ) ){
			monster.attackCoreStone();
		}
	},
});

Monsters.SPACE ={
	FRONT: 20,
	BACK: 0,
};
MonstersLink = null;