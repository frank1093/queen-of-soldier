var StartScreen = cc.Layer.extend({
	init: function(){
		cc.AudioEngine.getInstance().playMusic( 'sounds/SleepyWood.wav', 1);
		var startScreenPic = cc.Sprite.create( 'images/start_screen.png');
        startScreenPic.setAnchorPoint( 0, 0);
        this.addChild( startScreenPic );

        this.startText = cc.LabelBMFont.create(  'Press on the screen','images/player_status.fnt');
        this.startText.setAnchorPoint( 0.5 , 0);
        this.startText.setPosition( cc.p( GameLayer.WIDTH/2, 120 ) );
        this.startText.setVisible( false );
        this.addChild( this.startText);

        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 1.5 ) );
        actions.push( cc.FadeOut.create( 1.5 ) );
        this.startText.runAction( cc.RepeatForever.create(cc.Sequence.create( actions )) );

        this.setTouchEnabled( true );
        this.setKeyboardEnabled( true );
	},
	onKeyDown: function() {
		cc.AudioEngine.getInstance().playEffect( 'sounds/Enter.wav');
		changeToGameLayer( this );
	},
	onTouchesBegan: function() {
		cc.AudioEngine.getInstance().playEffect( 'sounds/Enter.wav');
		changeToGameLayer( this );
	}




});
        