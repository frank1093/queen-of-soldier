var ScoreLayer = cc.LayerColor.extend({
    init: function() {
        this._super(new cc.Color4B( 32, 32, 32, 255 ) );
        cc.AudioEngine.getInstance().playMusic( 'sounds/ComeWithMe.wav', 1);
        this.setVisible( false );
        this.setTouchEnabled( true );
        this.setKeyboardEnabled(true);
        this.setPosition( new cc.Point( 0, 0 ) );

        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        this.runAction( cc.Sequence.create( actions ) );

        this.showYourScoreText();
        this.showScoreText();
        // var item = cc.MenuItemAtlasFont.create('AA', 'images/game_start.fnt');
        // var item = cc.MenuItemSprite.create( cc.Sprite.create('images/1.png'), cc.Sprite.create('images/2.png'));
        // var item = cc.MenuItemAtlasFont.create('123', '/player_status.fnt', 12, 32, 'A');

        // var item =  cc.MenuItemToggle.create(cc.MenuItemFont.create("On"),cc.MenuItemFont.create("Off"));
        // item.setPosition( 0 , 100);
        // this.addChild( item );
        // this.setTouchEnabled( true );
        var mainMeunButton = cc.MenuItemFont.create("Main Meun", function(){ 
            cc.AudioEngine.getInstance().playEffect( 'sounds/Enter.wav');
            changeToMainMenu( this ); }, this);
        mainMeunButton.setFontSize(40);
        mainMeunButton.setFontName("Arial");
        mainMeunButton.setPosition( -250 , -150);
        // this.addChild( item3 );
        var playAgainButton = cc.MenuItemFont.create("Play Again", function(){ 
            cc.AudioEngine.getInstance().playEffect( 'sounds/Enter.wav');
            changeToGameLayer( this ); } , this);
        playAgainButton.setFontSize(40);
        playAgainButton.setFontName("Arial");
        playAgainButton.setPosition( 20 , -150);

        var shareButton = cc.MenuItemFont.create("Share", this.checkLoginState, this);
        shareButton.setFontSize(40);
        shareButton.setFontName("Arial");
        shareButton.setPosition( 250, -150);


        var s = cc.Menu.create(  mainMeunButton, playAgainButton, shareButton);
        s.runAction( cc.FadeIn.create( 2.5 ) );
        s.setTouchEnabled( true );
        this.addChild(s );

        console.log(this.isTouchEnabled());


    },
    onTouchesBegan:function() {
        console.log('dddd');
    },
    showYourScoreText: function() {
        this.createYourScoreText();
        var actions = [];
        actions.push( cc.DelayTime.create( 2.5 ));
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        this.yourScoreText.runAction( cc.Sequence.create( actions ) );
    },
    showScoreText: function(){
        this.createScoreText();
        var actions = [];
        actions.push( cc.DelayTime.create( 3.5 ));
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        this.scoreText.runAction( cc.Sequence.create( actions ) );
    },
    createYourScoreText: function() {
        this.yourScoreText = cc.LabelBMFont.create(  'YOUR SCORE','images/score2.fnt');
        this.yourScoreText.setPosition( GameLayer.WIDTH/2 , GameLayer.HEIGHT/2+100);
        this.yourScoreText.setVisible( false );
        this.addChild( this.yourScoreText );
    },
    createScoreText: function() {
        this.scoreText = cc.LabelBMFont.create(  GameStatusLink.mark,'images/score2.fnt');
        this.scoreText.setPosition( GameLayer.WIDTH/2 , GameLayer.HEIGHT/2);
        this.scoreText.setVisible( false );
        this.addChild( this.scoreText );
    },
    checkLoginState: function(){
          function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      // testAPI( response );

FB.login(function(){
 FB.api('/me/feed', 'post', {message: 'Your score: '+ GameStatus.mark});
}, {scope: 'publish_actions'});
 // FB.logout(function(response) {
 //        // Person is now logged out
 //    });
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1430581420524937',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


        
    }




}); 




var ScoreScene = cc.Scene.extend({
    ctor: function() {
        this._super();
        var layer = new ScoreLayer();
        layer.init();
        this.runAction( cc.FadeIn.create( 2 ) );
        this.addChild( layer );
    }
});

var changeToGameLayer = function( node ){
        cc.AudioEngine.getInstance().stopMusic();
        var background = cc.LayerColor.create( new cc.Color4B( 32, 32, 32, 255 ) );
        background.setVisible( false );
        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        actions.push(cc.CallFunc.create( function(){
            var director = cc.Director.getInstance();
            director.replaceScene( new GameLayerScene() );
        },this ));

        background.runAction( cc.Sequence.create( actions ) );
        node.addChild( background );
};

var changeToMainMenu = function( node ){
        var background = cc.LayerColor.create( new cc.Color4B( 32, 32, 32, 255 ) );
        background.setVisible( false );
        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        actions.push(cc.CallFunc.create( function(){
            var director = cc.Director.getInstance();
            director.replaceScene( new StartScene() );
        },this ));

        background.runAction( cc.Sequence.create( actions ) );
        node.addChild( background );
};

var changeToScoreLayer = function( node ){
        var background = cc.LayerColor.create( new cc.Color4B( 32, 32, 32, 255 ) );
        background.setVisible( false );
        var actions = [];
        actions.push( cc.Show.create() );
        actions.push( cc.FadeIn.create( 2 ) );
        actions.push(cc.CallFunc.create( function(){
            var director = cc.Director.getInstance();
            director.replaceScene( new ScoreScene() );
        },this ));

        background.runAction( cc.Sequence.create( actions ) );
        node.addChild( background );
};