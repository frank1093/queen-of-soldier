var StoneBug = cc.NodeRGBA.extend({
	ctor: function() {
		this._super();
		this.init();


		this.walkAction();
		this.move();
        

        this.scheduleUpdate();
	},
	update: function() {
		for ( var i=0; i< MonstersLink.annies.length; i++){
			var annie = MonstersLink.annies[i];
			if ( cc.rectOverlapsRect( annie.getRect(), this.getRect() ) && !annie.lockBackForStoneBug){
				// MonstersLink.annies[i].runAction( cc.MoveBy.create( 0.1, cc.p(10,0)));
				console.log("eeee");
				annie.lockBackForStoneBug = true;
				if ( !annie.lockStopActionsForStoneBug ){
					annie.lockStopActionsForStoneBug = true;
					annie.annieSprite.stopAllActions();
					// annie.annieSprite.stopAction( )
					annie.annieSprite.runAction( annie.movingAction );
					
					var actions = [];
					actions.push( cc.DelayTime.create( 1 ) );
					actions.push( cc.CallFunc.create( function(){ this.lockStopActionsForStoneBug = false;}, MonstersLink.annies[i] ) );
					actions.push( cc.CallFunc.create( annie.run , annie ) );
					annie.annieSprite.runAction( cc.Sequence.create( actions ) );
				}
				var actions = [];
				var ss = MonstersLink.annies[i];
				actions.push( cc.CallFunc.create( function(){ this.runAction( cc.MoveBy.create( 0.1, cc.p( 10,0) ) );}, MonstersLink.annies[i].annieSprite ) ) ;
				actions.push( cc.DelayTime.create( 0.1 ) );
				actions.push( cc.CallFunc.create( function(){ this.lockBackForStoneBug = false;}, MonstersLink.annies[i]) );
				var sequenceAction = cc.Sequence.create( actions );
				annie.runAction( sequenceAction );
			}
		}

	},
	getRect: function () {
		return this.stoneBugSprite.getBoundingBoxToWorld();
	},
	walkAction: function() {
		var animFrames = createAnimeFrames( 'StoneBug_Walk', 1, 17 );
        var animation = cc.Animation.create(animFrames, 0.1);
        this.movingAction = cc.RepeatForever.create(cc.Animate.create(animation));
        this.stoneBugSprite.runAction( this.movingAction );
        this.stoneBugSprite.runAction( cc.FadeIn.create( 1 ) );
	},
	move: function() {
		var actions = [];
		// this.stoneBugSprite.stopAllActions();
		actions.push( cc.MoveBy.create( Player.TIME.CALL_MINIONS, cc.p( Player.SKILL.CALL_MINIONS,0) ) );
		actions.push( cc.CallFunc.create( function(){ 
			this.stoneBugSprite.stopAction( this.movingAction );
			this.stoneBugSprite.runAction( this.createDeadAction() );}, this ) );
		actions.push( cc.FadeOut.create( 1) );
		actions.push( cc.CallFunc.create( this.removeFromParent, this) );
        this.stoneBugSprite.runAction( cc.Sequence.create( actions ) );
	},
	init: function() {
		cc.SpriteFrameCache.getInstance().addSpriteFrames('images/StoneBug.plist');
		this.stoneBugSprite = cc.Sprite.createWithSpriteFrameName("StoneBug_Walk1.png");
		this.stoneBugSprite.setAnchorPoint( 0.5, 0 );
		this.stoneBugSprite.setFlippedX( true );
		this.addChild( this.stoneBugSprite );
	},
	createDeadAction: function() {
		var animFrames = createAnimeFrames( 'StoneBug_Dead', 1, 9 );
        var animation = cc.Animation.create(animFrames, 0.1);
        var deadingAction = cc.Repeat.create(cc.Animate.create( animation ), 1 );
        return deadingAction;
	}

});