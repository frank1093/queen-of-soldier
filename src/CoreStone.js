var CoreStone = cc.Node.extend({
	ctor: function() {
		this._super();
		CoreStoneLink = this;
		// this.initWithFile( 'images/Core_Stone.png' );
		// this.setAnchorPoint(0,0);
		this.coreStone =  cc.Sprite.create( 'images/Core_Stone.png' );
		this.coreStone.setAnchorPoint(0,0);
		this.coreStone.setPosition( cc.p( 0,70+70));
        this.setZOrder( 0 );
		this.addChild( this.coreStone );

		this.healthPoint = new HealthPoint( 5000 );
		this.healthPoint.setFullHeathPoint();
		this.healthPoint.setPosition( cc.p(80,60) );
		this.coreStone.addChild( this.healthPoint );

		this.loss = false;

	},
	getRect: function() {
		return this.coreStone.getBoundingBoxToWorld();
	},
	deductHealthPoint: function( damage ,damageBase , damageRange) {
		cc.AudioEngine.getInstance().playEffect( 'sounds/10001066.Hit.wav');
		this.healthPoint.reduce( damage );
		// this.healthPoint.HP.getPercentage();
		var colorCode =  255*this.healthPoint.HP.getPercentage()/100+80;
		
		this.coreStone.setColor( new cc.Color3B( colorCode, colorCode, colorCode) );
		var damage = new Damage( damage , this.getMaxX() -150 + Math.random()*60, (this.getMinY()+this.getMaxY())/2 + Math.random()*40,damageBase,damageRange );
		damage.setZOrder( 2 );
		this.addChild( damage );
		// console.log( this.healthPoint.HP.getPercentage() );
		if ( this.healthPoint.HP.getPercentage()  == 0 && !this.loss){
			this.loss = true;
			GameStatusLink.pauseSchedulerAndActions();
			GameStatusLink.showGameOver();
		}
	},
	getMaxX: function() {
		return cc.rectGetMaxX( this.getRect() );
	},
	getMinX: function() {
		return cc.rectGetMinX( this.getRect() );
	},
	getMaxY: function() {
		return cc.rectGetMaxY( this.getRect() );
	},
	getMinY: function() {
		return cc.rectGetMinY( this.getRect() );
	},


});

CoreStoneLink = null;