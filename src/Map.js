var Map = cc.Node.extend({
	ctor: function() {
		this._super();
		MapLink = this;
		this.WIDTH = 14;
		this.HEIGHT = 6;
		this. MAP = [
		'##############',
		'..............',
		'..............',
		'..............',
		'..............',
		'##############'
		];

		this.walls = [];
		this.streets = [];
		for (var i=0;i<this.WIDTH;i++){
			for (var j=0;j<this.HEIGHT;j++){
				if (this.MAP[j][i] == '#'){
					var wall = new Wall();
					wall.setPosition( new cc.Point(i*70,j*70));
					// wall.setPosition( new cc.Point(0,0));
					wall.setAnchorPoint( cc.p( 0, 0 ) );
					this.walls.push(wall);
					this.addChild(wall);
				}
				else if ( this.MAP[j][i] == '.' ){
					var street = new Street();
					street.setPosition( new cc.Point(i*70,j*70) );
					street.setAnchorPoint( cc.p( 0, 0) );
					this.streets.push( street );
					this.addChild( street );
				}
			}
		}
	},
	getUnderWallTopY: function () {
		return cc.rectGetMaxY( this.walls[0].getBoundingBoxToWorld() );
	},
	getAboveWallTopY: function() {
		return cc.rectGetMaxY( this.walls[this.WIDTH+1].getBoundingBoxToWorld() );
	},
	getAboveWallLowY: function() {
		return cc.rectGetMinY( this.walls[this.WIDTH+1].getBoundingBoxToWorld() );
	},
	getUnderWallLowY: function() {
		return cc.rectGetMinY( this.walls[0].getBoundingBoxToWorld() );
	},
	getUnderWallRect: function() {
		return this.walls[0].getBoundingBoxToWorld();
	},
	getAboveWallRect: function() {
		return this.walls[this.WIDTH+1].getBoundingBoxToWorld();
	},
	// hitTop: function ( oldRect, newRect) {
	// 	if ( cc.rectGetMinY( oldRect ) >= this.getUnderWallTopY() ){
	// 		var uRect = cc.rectUnion( oldRect, newRect );
	// 		return cc.rectIntersectsRect( uRect, this.getUnderWallTopY() );
	// 	}
	// },
	// onTop: function() {

	// }
	hitUnderWall: function ( newRectPlayer ){
		var playerMinY = cc.rectGetMinY(newRectPlayer);
		if ( playerMinY < this.getUnderWallTopY() ){
			return true;
		}
		return false;
	},
	hitAboveWall: function ( newRectPlayer ){
		var playerMinY = cc.rectGetMinY(newRectPlayer);
		if ( playerMinY >= this.getAboveWallLowY() ){
			return true;
		}
		return false;
	},
	// createStoneBug: function() {
	// 	this.bugStones = [];
	// 	for (var j=1;j<this.HEIGHT-1;j++){
	// 		var bugStone = new StoneBug();
	// 		this.bugStones.push( bugStone );
	// 		bugStone.setPosition( cc.p( 4*70,j*70))
	// 		this.addChild( bugStone );
	// 	}
	// }
});

Map.LINE = {
	FIRST: 70+70,
	SECOND: 140+70,
	THRID: 210+70,
	FOURTH: 280+70,
};

Map.ZOrder = {
	FIRSTLINE: 4,
	SECONDLINE: 3,
	THRIDLINE: 2,
	FOURTHLINE: 1
};

Map.DISTANCE = 70;
Map.WIDTH = 14;
Map.HEIGHT= 6;

MapLink = null;
