var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color4B( 127, 127, 127, 255 ) );
        // cc.AudioEngine.getInstance().playMusic( 'sounds/BlueSky.wav', 1);
        this.setPosition( new cc.Point( 0, 0 ) );
        GameLayerLink = this;

        this.map = new Map();
        this.map.setPosition(0,70);
        // this.setAnchorPoint( cc.p( 0, 0 ) );

        this.addChild(this.map);

        this.player = new Player();
        // this.player.setPosition( new cc.Point( 20,70));
        // this.player.setAnchorPoint(0,0);
        this.addChild( this.player );
        this.player.setMap( this.map );

        this.monsters = new Monsters();
        this.addChild( this.monsters );
        this.player.setMonsters( this.monsters );

        this.coreStone = new CoreStone();
        this.addChild( this.coreStone );

        this.gameStatus = new  GameStatus();
        this.addChild( this.gameStatus );

        this.lockCheckOutOfBound = false;
        this.setKeyboardEnabled( true );

        // PlayerLink.createStoneBug();
        this.setUpBackgroundMusicByTime();

        return true;
    },
    update: function ( dt ){
    //     if (!this.lockCheckOutOfBound)
    //     this.checkOutOfMap();
    },
    onKeyDown: function( key ){
        // console.log(this.map.getUnderWallLowY());
        // console.log(this.map.getAboveWallTopY());
        // console.log(this.player.getPositionY());
        // this.lockCheckOutOfBound = false;
        // if ( !this.player.checkHitWall() ){
            // console.log(key);
        switch (key){

            case cc.KEY.up: this.player.setDirection(Player.DIR.UP);break;
            case cc.KEY.down: this.player.setDirection(Player.DIR.DOWN);break;
            case cc.KEY.right: this.player.setDirection(Player.DIR.RIGHT);break;
            case cc.KEY.left: this.player.setDirection(Player.DIR.LEFT);break;
            case cc.KEY.c: this.player.holdAttack = true;break;
            case cc.KEY.s: this.player.useCallMinionsSkill();break;
            case cc.KEY.a: this.player.useRecoverHPPlayerSkill();break;
            case cc.KEY.d: this.player.useAttackSpeedSkill();break;
            case cc.KEY.z: this.player.useAttackSkill();break;
            case cc.KEY.x: this.player.useDefenseSkill();break;
            case cc.KEY.q: this.player.upgradeAttack();break;
            case cc.KEY.w: this.player.upgradeDefence();break;
            case cc.KEY.e: this.player.upgradeAttackRange();break;
        // }
    }
    },
    onKeyUp: function() {
        this.player.showStandingState();
    },
    setUpBackgroundMusicByTime: function(){
        this.schedule( function(){
            cc.AudioEngine.getInstance().stopMusic();
            var choice = Math.floor(Math.random()*11) + 1;
            switch( choice ){
                case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/BlueSky.wav', 1);break;
                case 2: cc.AudioEngine.getInstance().playMusic( 'sounds/PantheonField.wav', 1);break;
                case 3: cc.AudioEngine.getInstance().playMusic( 'sounds/BaseOfBetrayers.wav', 1);break;
                case 4: cc.AudioEngine.getInstance().playMusic( 'sounds/BorderArea.wav', 1);break;
                case 5: cc.AudioEngine.getInstance().playMusic( 'sounds/StepOfKaiser.wav', 1);break;
                case 6: cc.AudioEngine.getInstance().playMusic( 'sounds/TrappedKaiser.wav', 1);break;
                case 7: cc.AudioEngine.getInstance().playMusic( 'sounds/NetsPiramid.wav', 1);break;
                case 8: cc.AudioEngine.getInstance().playMusic( 'sounds/GhostShip.wav', 1);break;
                case 9: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                case 10: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonNest.wav', 1);break;
                case 11: cc.AudioEngine.getInstance().playMusic( 'sounds/Beachway.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;
                // case 1: cc.AudioEngine.getInstance().playMusic( 'sounds/DragonRider.wav', 1);break;

            }
        }, 60, cc.RepeatForever, 2.5);


    },
});

GameLayer.WIDTH = 980;
GameLayer.HEIGHT = 420;
GameLayerLink = null;

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var startScreen = new StartScreen();
        startScreen.init();

        var background = cc.LayerColor.create( new cc.Color4B( 32, 32, 32, 255 ) );
        // background.setVisible( false );
        var actions = [];
        // actions.push( cc.Show.create() );
        actions.push( cc.FadeOut.create( 2 ) );
        actions.push( cc.CallFunc.create( this.removeFromParent, this, true ) );
        background.runAction( cc.Sequence.create( actions ) );

        this.addChild( startScreen );
        this.addChild( background );
    }
});

var GameLayerScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();

        var background = cc.LayerColor.create( new cc.Color4B( 32, 32, 32, 255 ) );
        // background.setVisible( false );
        var actions = [];
        // background.push( cc.Show.create() );
        actions.push( cc.FadeOut.create( 2 ) );
        actions.push( cc.CallFunc.create( this.removeFromParent, this, true ) );
        background.runAction( cc.Sequence.create( actions ) );

        this.addChild( layer );
        this.addChild( background );
    }
});

