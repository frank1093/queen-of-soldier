var HealthPoint = cc.Sprite.extend({
	ctor: function( maxHealthPoint ){
		this._super();
		this.initWithFile( 'images/Red_HP_Bar_Frame.png')
		this.setContentSize( 0, 0);
		this.HP = cc.ProgressTimer.create( cc.Sprite.create('images/Red_HP_Bar.png') );
		// this.initWithSprite( cc.Sprite.create('images/Red_HP_Bar.png') );
		this.HP.setType( cc.PROGRESS_TIMER_TYPE_BAR );
		// this.setPercentage( 100 );
		// this.draw();
		// this.setAnchorPoint( 0.2, 0.4);
		this.HP.setContentSize( 0, 0 )
		// this.setType( cc.PROGRESS_TIMER_TYPE_BAR );
		// this.setType(1);
		// console.log( this.getBarChangeRate() );
		this.HP.setMidpoint( cc.p( 0, 0) );
		// this.setReverseProgress( true );
		// this.setReverseDirection( true );
		// this.setPosition( cc.p( 100, 70) );
		this.HP.setBarChangeRate( cc.p( 1, 0 ) );

		this.maxHealthPoint = maxHealthPoint;
		this.healthPoint = maxHealthPoint;
		this.recoverHealthPoint = 0;
		this.increaseHealthPointAction = null;


		this.redColor = new cc.Color3B( 204, 0, 0);
		this.orangeColor = new cc.Color3B( 204, 102, 0);
		this.greenColor = new cc.Color3B( 0, 102, 0 );
		// this.transparentColor = new cc.Color3B( 255, 255, 255 );
		this.HP.setColor( this.greenColor );
		this.addChild( this.HP );

		// this.HP.setPercentage( 100 );

	},
	reduce: function( lossNumber ){
		if ( this.increaseHealthPointAction ){
			if ( !this.increaseHealthPointAction.isDone() ){
				this.HP.stopAllActions();
				this.healthPoint -= this.recoverHealthPoint/2;
			}
		}
		
		
		this.healthPoint -= lossNumber;
		if ( this.healthPoint < 0 ){
			this.healthPoint = 0;
		}
		var oldPercentHealthPoint = this.HP.getPercentage();
		var newPercentHeathPoint = this.healthPoint/this.maxHealthPoint *100;
		var action = cc.ProgressFromTo.create( 0.3, oldPercentHealthPoint, newPercentHeathPoint );
		// var action = cc.ProgressTo.create( 0.3, newPercentHeathPoint );
		this.HP.runAction( action );
		// console.log( this.getPercentage() +"EEEEEEEEEEEEEEEEEEEEEEEE");
		this.changeColor( newPercentHeathPoint );
		

	},
	getHealthPoint: function() {
		return this.healthPoint;
	},
	setFullHeathPoint: function() {
		// this.HP.setPercentage( 100 );
		var action = cc.ProgressTo.create( 0.3, 100);
		this.HP.runAction( action );

		this.healthPoint = this.maxHealthPoint;
		// this.stopAllActions();
		this.HP.setColor( this.greenColor );
	},
	changeColor: function( percent ) {
		// if ( this.getPercentage() < 25 ){
		// 	this.setColor( this.redColor );
		// }
		// else if ( this.getPercentage() < 75 ) {
		// 	this.setColor( this.orangeColor );
		// }
		if ( percent < 25 ){
			this.HP.setColor( this.redColor );
		}
		else if ( percent < 50 ) {
			this.HP.setColor( this.orangeColor );
		}
		else {
			this.HP.setColor( this.greenColor );
		}
	},
	increase: function( number , time) {
		var oldPercentHealthPoint = this.HP.getPercentage();
		this.healthPoint += number;
		this.recoverHealthPoint = number;

		var newPercentHeathPoint = this.healthPoint/this.maxHealthPoint *100;
		// var action = cc.ProgressFromTo.create( time, oldPercentHealthPoint, newPercentHeathPoint );
		this.increaseHealthPointAction = cc.ProgressTo.create( time, newPercentHeathPoint );
		this.HP.runAction( this.increaseHealthPointAction );
		// console.log( this.HP.getPercentage() +"..." + oldPercentHealthPoint);
		this.changeColor( newPercentHeathPoint );
	}



});