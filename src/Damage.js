var Damage = cc.Node.extend({
	ctor: function( damage, x, y, baseDamge, damageRange) {
		this._super();
        this.setVisible(false);
		this.damage = damage;
        this.baseDamge = baseDamge;
        this.damageRange = damageRange;
		this.digit = this.damage.length;
		this.x = x;
		this.y = y;
        this.setAnchorPoint( 0.5, 0 );
        this.setZOrder(6);
		this.init();

	},
	init:function () {

	    cc.SpriteFrameCache.getInstance().addSpriteFrames('images/damage.plist');

	    this.createShowingAction();
    },
    createShowingAction: function() {

        this.runAction( cc.Show.create());
        this.rangX = -10;
        this.rangY = 40;
        
        var background = cc.Sprite.createWithSpriteFrameName( "background.png");
        var damage =  cc.LabelBMFont.create(  Math.round(this.damage), 'images/number_damage.fnt' );
        damage.setAnchorPoint( 0.5, 0.5 );
        damage.setPosition( this.x+this.rangX, this.y+this.rangY);
        damage.setScaleX( 0.8 );
        damage.setZOrder( 2 );
        damage.runAction( cc.FadeIn.create(0.1) );
        this.addChild( damage );
        // console.log(  this.x +"    "+ this.y );
        if ( (this.damage-this.baseDamge)/this.damageRange*100 >= 75 ){
            background.setPosition( cc.p( this.x+this.rangX, this.y+this.rangY) );
            background.setAnchorPoint( 0.5 , 0.45 );
            background.setZOrder( 1 );
            
            var action = cc.FadeIn.create(0.1);
            background.runAction( action );
            this.addChild( background );
        }

        this.remove();

    },
    remove: function() {
        this.getChildren().forEach( function(i) {
            i.runAction( cc.JumpBy.create(0.5 , cc.p( 0,0)  , 20,1));
            i.runAction( cc.ScaleBy.create(1,1.5, 1.5));

            var action = cc.FadeOut.create(0.3);
            i.runAction( cc.Sequence.create(cc.DelayTime.create(0.1), action ));
        });

        var finish = cc.Sequence.create(cc.DelayTime.create(1),  cc.CallFunc.create(this.removeFromParent , this ,true));
        this.runAction( finish );
    }


});